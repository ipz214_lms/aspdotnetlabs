﻿using System.Text.Json;

namespace AspDotNetLab2
{
    public interface IGeneralCounterService
    {
        void IncrementCounter(string url);
        int GetCounter(string url);
        IDictionary<string, int> GetCounts();
    }

    public class GeneralCounterService : IGeneralCounterService
    {

        private readonly Dictionary<string, int> _counters = new Dictionary<string, int>();

        public void IncrementCounter(string url)
        {
            if (!_counters.ContainsKey(url))
            {
                _counters[url] = 0;
            }
            _counters[url]++;
        }

        public int GetCounter(string url)
        {
            if (_counters.ContainsKey(url))
            {
                return _counters[url];
            }
            return 0;
        }
        
        public IDictionary<string, int>  GetCounts()
        {
            return new Dictionary<string, int>(_counters);
        }
    }

}

