﻿namespace AspDotNetLab2
{
    public interface IRandomService
    {
        int Value { get; }
    }
    public class RandomService : IRandomService
    {
        private static Random rnd = new Random();
        private int _value;
        public RandomService()
        {
            _value = rnd.Next(0, 1000000);
        }
        public int Value
        {
            get => _value;
        }

    }
}
